from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=30, verbose_name='Название')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Company(models.Model):
    description = models.CharField(max_length=50, verbose_name='Описание')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.description

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'


class Product(models.Model):
    title = models.CharField(max_length=30, verbose_name='Название')
    description = models.CharField(max_length=30, verbose_name='Описание')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='product_category',
                                 verbose_name='Категория')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='product_company',
                                verbose_name='Компания')
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
