from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.response import Response
from rest_framework.filters import SearchFilter

from django_filters.rest_framework import DjangoFilterBackend

from .models import Category
from .models import Company
from .models import Product

from .serializers import CategorySerializer
from .serializers import CompanySerializer
from .serializers import ProductSerializer


class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    authentication_classes = [JSONWebTokenAuthentication]

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'update' or self.action == 'destroy':
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]


class CompanyViewSet(ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    authentication_classes = [JSONWebTokenAuthentication]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Company.objects.filter(is_active=True))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'update' or self.action == 'destroy':
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]


class ProductViewSet(ModelViewSet):
    filter_backends = (DjangoFilterBackend, SearchFilter,)
    filter_fields = ('company', 'category',)
    search_fields = ('title',)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    authentication_classes = [JSONWebTokenAuthentication]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Product.objects.filter(is_active=True))
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create' or self.action == 'update' or self.action == 'destroy':
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]
