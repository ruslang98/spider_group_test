from django.contrib import admin

from .models import Category
from .models import Company
from .models import Product


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('description', 'is_active')
    list_filter = ('description', 'is_active')
    search_fields = ('description', 'is_active')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'category', 'company', 'is_active')
    list_filter = ('title', 'description', 'category', 'company', 'is_active')
    search_fields = ('title', 'description')
    ordering = ['title']


admin.site.register(Category)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Product)
