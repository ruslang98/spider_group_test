from rest_framework import routers
from .views import CategoryViewSet, CompanyViewSet, ProductViewSet


app_name = 'products'

router = routers.SimpleRouter()
router.register(r"categories", CategoryViewSet, basename='categories')
router.register(r"companies", CompanyViewSet, basename='companies')
router.register(r"products", ProductViewSet, basename='products')

urlpatterns = router.urls
