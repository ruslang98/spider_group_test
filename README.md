### Installation

Install the dependencies.

```sh
$ pip3 install -r requirements.txt
```

Don't forget migrations.

```sh
$ python3 manage.py makemigrations
$ python3 manage.py migrate
```

### Run development server

If you want to start development server without docker.

```sh
$ python3 manage.py runserver
```


### Docker

By default, the Docker will expose port 8000.

```sh
$ sudo docker-compose build
$ sudo docker-compose up
```

### Swagger

http://127.0.0.1:8000/docs/

### Django admin

If you want to enter django admin you should create super user.

```sh
$ python3 manage.py createsuperuser
```


