from django.urls import path
from .views import SignupUserAPIView, LoginUserAPIView

app_name = 'users'

urlpatterns = [
    path('auth/signup/', SignupUserAPIView.as_view()),
    path('auth/signin/', LoginUserAPIView.as_view()),
]
