from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response
from rest_framework_jwt.utils import jwt
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema

from spider_group_test import settings

from .models import User
from .serializers import UserSerializer

from .services import authenticate, email_validation


JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER


class SignupUserAPIView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(operation_description="Signup user", request_body=UserSerializer,
                         responses={201: 'Created', 406: 'Not Acceptable', 400: 'Bad request'})
    def post(self, request):

        first_name = request.data.get("first_name")
        last_name = request.data.get("last_name")
        password = request.data.get("password")
        username = request.data.get("username")
        email = request.data.get("email")
        phone = request.data.get("phone")

        if not (first_name and last_name and password and username and email and phone):
            return Response(
                data={
                    "detail": "Need dictionary {first_name:<first_name>, last_name:<last_name>, "
                              "username: <username>, email:<email>, password:<password>, phone:<phone>} "
                },
                status=status.HTTP_406_NOT_ACCEPTABLE,
            )

        if User.objects.filter(username=username).exists():
            return Response(data={"detail": "This user already exists"}, status=status.HTTP_400_BAD_REQUEST)

        email_validation(email)

        new_user = User.objects.create_user(username=username, password=password, first_name=first_name,
                                            last_name=last_name, email=email, phone=phone)
        return Response(
            data={
                "id": new_user.id,
                "username": new_user.username,
                "first_name": new_user.first_name,
                "last_name": new_user.last_name,
                "email": new_user.email,
                "phone": new_user.phone
            },
            status=status.HTTP_201_CREATED)


class LoginUserAPIView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request):

        """
               Login user. Returns Bearer token.

               Request example:
              ```
              Request value:
                {
                    "username": "username",
                    "password": "password",
                }

        """

        username = request.data.get("username")
        password = request.data.get("password")

        if not (username and password):
            return Response(
                data={"detail": "Need dictionary {username:<username>, password:<password>}"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        user = authenticate(username=username, password=password)

        if not user:
            return Response(data={"detail": "username or password incorrect"}, status=status.HTTP_401_UNAUTHORIZED)

        payload = JWT_PAYLOAD_HANDLER(user)
        token = jwt.encode(payload, settings.SECRET_KEY)
        return Response(
            data={
                "id": user.id,
                "username": user.username,
                "token": token,
                "token_type": "Bearer"
            }, status=status.HTTP_200_OK)
