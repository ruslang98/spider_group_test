from .models import User

from django.core.validators import EmailValidator
from django.core.exceptions import ValidationError

from rest_framework.response import Response


def authenticate(username=None, password=None):
    if username is None:
        return None
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        return None

    if user.check_password(password):
        return user


def email_validation(email):
    try:
        email_validator = EmailValidator()
        email_validator(email)
    except ValidationError:
        return Response(data={"detail": f"{email} has validation error"}, status=406)





