from django.db import models
from django.contrib.auth.models import PermissionsMixin, AbstractUser

from rest_framework.authtoken.models import Token


class User(AbstractUser):

    first_name = models.CharField(max_length=30, null=True, verbose_name='Имя')
    last_name = models.CharField(max_length=30, null=True, verbose_name='Фамилия')
    username = models.CharField(max_length=30, unique=True, verbose_name='Логин')
    email = models.EmailField(unique=True, verbose_name='Электронная почта')
    phone = models.CharField(max_length=30, verbose_name='Телефон')

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

