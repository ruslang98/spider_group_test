from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns_doc = [
    re_path(r'^docs(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0),
            name='schema-json'),
    re_path(r'^docs/$',
            schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'),
    re_path(r'^redocs/$',
            schema_view.with_ui('redoc', cache_timeout=0),
            name='schema-redoc'),
]


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/users/', include(('users.urls_v1', 'users'), namespace='users')),
    path('api/v1/', include(('products.urls_v1', 'products'), namespace='products'))
]


if settings.DEBUG:
    urlpatterns.extend(urlpatterns_doc)

